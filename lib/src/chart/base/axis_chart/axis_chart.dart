

import 'package:line_chart/src/chart/base/base_chart/base_chart.dart';

/// This class is suitable for axis base charts
/// in the axis base charts we have a grid behind the charts
/// the direct subclasses are [LineChart]
abstract class AxisChart extends BaseChart {}
