import 'package:flutter/material.dart';
import 'package:line_chart/src/chart/base/axis_chart/axis_chart_data.dart';
import 'package:line_chart/src/chart/line_chart/line_chart.dart';
import 'package:line_chart/src/chart/line_chart/line_chart_data.dart';

import '../line_chart.dart';

class LineChartView extends StatelessWidget {
  List<Color> barAreaColors;
  Color lineColor;
  TextStyle textStyle;
  List<FlSpot> liSpot;
  double? barWidth;
  bool? isHideTouchBar;
  dynamic min;
  dynamic max;

  LineChartView({
    required this.barAreaColors,
    required this.lineColor,
    required this.textStyle,
    required this.liSpot,
    this.barWidth,
    this.isHideTouchBar = false,
  });

  @override
  Widget build(BuildContext context) {
    min = liSpot[0].y;
    max = liSpot[0].y;
    for (int i = 0; i< liSpot.length; i++) {
      if (min > liSpot[i].y) min = liSpot[i].y;
      if (max < liSpot[i].y) max = liSpot[i].y;
    }
    return LineChart(
      _buildChartData()
    );
  }

  LineChartData _buildChartData() {
    return LineChartData(
      // lineTouchData: LineTouchData(enabled: true,),
      lineTouchData: LineTouchData(
        enabled: true,
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.transparent,
          tooltipPadding: const EdgeInsets.symmetric(
            vertical: 5,
            horizontal: 8,
          ),
          //tooltipMargin: 25,
          getTooltipItems: (List<LineBarSpot> touchedBarSpot) {
            return touchedBarSpot.map((spot) {
              if (isHideTouchBar != true)
                return LineTooltipItem(
                  '${liSpot[spot.spotIndex].y} USDT',
                  textStyle,
                );
              return LineTooltipItem('', TextStyle());
            }).toList();
          },
        ),
      ),
      gridData: FlGridData(show: false),
      titlesData: FlTitlesData(show: false),
      borderData: FlBorderData(show: false),
      clipData: FlClipData.none(),
      minX: 0,
      minY: min - ((max - min) / 6),
      maxY: max,
      lineBarsData: [
        LineChartBarData(
          barWidth: barWidth,
          spots: liSpot,
          isCurved: true,
          color: lineColor,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            gradientFrom: Offset(0, 0),
            gradientTo: Offset(0, 1),
            colors: barAreaColors,
          ),
        ),
      ],
    );
  }
}
